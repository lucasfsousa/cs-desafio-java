package br.com.lucasfelix.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsDesafioJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsDesafioJavaApplication.class, args);
	}
}
