package br.com.lucasfelix.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.lucasfelix.desafio.vo.Mensagem;

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<Mensagem> businessException(BusinessException e){
		Mensagem msg = new Mensagem(e.getMessage());
		return new ResponseEntity<Mensagem>(msg, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InvalidAuthenticationException.class)
	public ResponseEntity<Mensagem> invalidAuthenticationException(InvalidAuthenticationException e){
		Mensagem msg = new Mensagem("Usuário e/ou senha inválidos");
		return new ResponseEntity<Mensagem>(msg, HttpStatus.UNAUTHORIZED);
	}
}
