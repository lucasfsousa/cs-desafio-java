package br.com.lucasfelix.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.lucasfelix.desafio.domain.UserDomain;

public interface UserRepository extends JpaRepository<UserDomain, String> {
	UserDomain findByEmail(String email);
}
