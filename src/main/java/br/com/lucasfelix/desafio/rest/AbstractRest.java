package br.com.lucasfelix.desafio.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractRest {
	public <T> ResponseEntity<T> responseOK(T body){
		return new ResponseEntity<T>(body, HttpStatus.OK);
	}
}
