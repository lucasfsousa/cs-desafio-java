package br.com.lucasfelix.desafio.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucasfelix.desafio.domain.UserDomain;
import br.com.lucasfelix.desafio.form.LoginForm;
import br.com.lucasfelix.desafio.service.LoginService;

@RestController
@RequestMapping("/login")
public class LoginRest extends AbstractRest {
	@Autowired
	private LoginService service;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<UserDomain> login(@Valid @RequestBody LoginForm form){
		return responseOK(service.login(form.getEmail(), form.getPassword()));
	}
}
