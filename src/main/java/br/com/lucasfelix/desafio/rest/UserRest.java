package br.com.lucasfelix.desafio.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucasfelix.desafio.domain.UserDomain;
import br.com.lucasfelix.desafio.service.UserService;

@RestController
@RequestMapping(value="/users")
public class UserRest extends AbstractRest {
	@Autowired
	private UserService service;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<UserDomain> postUser(@Valid @RequestBody UserDomain user){
		return responseOK(service.save(user));
	}
}
