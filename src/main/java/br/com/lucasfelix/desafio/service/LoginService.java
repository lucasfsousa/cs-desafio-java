package br.com.lucasfelix.desafio.service;

import br.com.lucasfelix.desafio.domain.UserDomain;

public interface LoginService {

	UserDomain login(String email, String password);

}
