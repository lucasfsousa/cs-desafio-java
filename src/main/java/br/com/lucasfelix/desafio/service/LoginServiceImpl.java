package br.com.lucasfelix.desafio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucasfelix.desafio.domain.UserDomain;
import br.com.lucasfelix.desafio.exception.InvalidAuthenticationException;
import br.com.lucasfelix.desafio.repository.UserRepository;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private UserRepository repository;
	
	@Override
	public UserDomain login(String email, String password) {
		UserDomain user = repository.findByEmail(email);
		
		if(user == null){
			throw new InvalidAuthenticationException();
		}
		else{
			if(!password.equals(user.getPassword())){
				throw new InvalidAuthenticationException();
			}
			else{
				return user;
			}
		}
	}

}
