package br.com.lucasfelix.desafio.service;

import java.util.List;

import br.com.lucasfelix.desafio.domain.UserDomain;

public interface UserService {
	UserDomain save(UserDomain user);
	List<UserDomain> listUsers();
}
