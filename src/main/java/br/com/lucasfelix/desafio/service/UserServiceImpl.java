package br.com.lucasfelix.desafio.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucasfelix.desafio.domain.UserDomain;
import br.com.lucasfelix.desafio.exception.BusinessException;
import br.com.lucasfelix.desafio.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository repository;
	
	@Override
	public UserDomain save(UserDomain user) {
		Date now = new Date();
		String token = UUID.randomUUID().toString();
		
		//Verify if the user exists
		if(repository.findByEmail(user.getEmail()) != null){
			throw new BusinessException("E-mail já existente");
		}
		else{
			//save the user data
			user.setToken(token);
			user.setLastLogin(now);
			user.setCreated(now);
			user.setModified(now);
			
			repository.save(user);
			
			//find the user by email
			return repository.findByEmail(user.getEmail());
		}
	}

	@Override
	public List<UserDomain> listUsers() {
		return repository.findAll();
	}
}
